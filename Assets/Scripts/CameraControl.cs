using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    [SerializeField] private Camera Main;

    [SerializeField] private GameController gameController;

    [SerializeField] private Character Player;

    [SerializeField] private float _speed = 10;

    [SerializeField] private float _NormalFOV = 60;
    [SerializeField] private float _BoostFOV = 10;

    private Coroutine CorCameraFOV;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnEnable()
    {
        GameController.EventGameStart += StartGame;
        GameController.EventFinishGame += FinishGame;

    }

    private void OnDisable()
    {
        GameController.EventGameStart -= StartGame;
        GameController.EventFinishGame -= FinishGame;
    }

    void StartGame()
    {

    }

    void FinishGame()
    {
        if (CorCameraFOV != null)
            StopCoroutine(CorCameraFOV);

        CorCameraFOV = StartCoroutine(FadeFOV(Main, 2f, _NormalFOV));
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!GameController.IsGame)
            return;

        transform.position = Vector3.MoveTowards(transform.position, Player.CameraPosition.position, Time.fixedDeltaTime * _speed);
        transform.LookAt(Player.CameraLookAt);

        if(Player.IsBoost)
        {
            if (CorCameraFOV != null)
                StopCoroutine(CorCameraFOV);

            CorCameraFOV = StartCoroutine(FadeFOV(Main, 2f, _BoostFOV));
        }
        else
        {
            if (CorCameraFOV != null)
                StopCoroutine(CorCameraFOV);

            CorCameraFOV = StartCoroutine(FadeFOV(Main, 2f, _NormalFOV));
        }
    }

    public IEnumerator FadeFOV(Camera Camera, float time, float value)
    {
        float currentTime = 0;
        float start = Camera.fieldOfView;

        while (currentTime < time)
        {
            currentTime += Time.deltaTime;
            Camera.fieldOfView = Mathf.Lerp(start, value, currentTime / time);
            yield return null;
        }

        yield break;
    }
}
