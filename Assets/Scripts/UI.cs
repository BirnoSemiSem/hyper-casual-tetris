using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public TMP_Text ScoreText;
    public Animator AnimBackground;
    public GameObject RestartBtn;

    private void OnEnable()
    {
        GameController.EventGameStart += StartGame;
        GameController.EventFinishGame += FinishGame;
        GameController.EventScoreUp += UpdateScore;
    }

    private void OnDisable()
    {
        GameController.EventGameStart -= StartGame;
        GameController.EventFinishGame -= FinishGame;
        GameController.EventScoreUp -= UpdateScore;
    }

    void StartGame()
    {
        AnimBackground.gameObject.SetActive(false);
        RestartBtn.SetActive(false);
    }

    void FinishGame()
    {
        AnimBackground.gameObject.SetActive(true);
        AnimBackground.Play("Background-In");

        RestartBtn.SetActive(true);
    }

    void UpdateScore(int score)
    {
        ScoreText.text = $"Score: {score}";
    }

    public void ClickRestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
