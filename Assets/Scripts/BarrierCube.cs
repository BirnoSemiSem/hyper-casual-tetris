using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierCube : MonoBehaviour
{
    [SerializeField] private Rigidbody _rb;

    [SerializeField] private float _powerKnockback = 10f;

    // Start is called before the first frame update
    void Start()
    {
        if (_rb == null)
            _rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!GameController.IsGame)
            return;

        if (_rb.useGravity)
            return;

        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<Character>().PlayerKnockback();
            _rb.useGravity = true;

            _rb.constraints = RigidbodyConstraints.None;

            _rb.AddForce(Vector3.forward * _powerKnockback, ForceMode.Impulse);
        }
    }
}
