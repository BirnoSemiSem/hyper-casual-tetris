using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneControl : MonoBehaviour
{
    enum Zone
    {
        Finish,
        Obstacles
    }

    [SerializeField] private Zone _zone;

    private GameController gameController;

    private void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameControl").GetComponent<GameController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!GameController.IsGame)
            return;

        if (other.gameObject.tag != "Player")
            return;

        switch (_zone)
        {
            case Zone.Finish:
            {
                gameController.FinishGame();
                break;
            }
            case Zone.Obstacles:
            {

                gameController.ScoreUp();
                break;
            }
        }
    }
}
