using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameController : MonoBehaviour
{
    [SerializeField] private int Score = 0;
    [SerializeField] private int AddScoreForLet = 30;

    public static bool IsGame = false;

    static public UnityAction EventGameStart;
    static public UnityAction EventFinishGame;

    static public UnityAction<int> EventScoreUp;

    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    public void StartGame()
    {
        IsGame = true;
        EventGameStart?.Invoke();
    }
    
    public void FinishGame()
    {
        IsGame = false;
        EventFinishGame?.Invoke();
    }

    public void ScoreUp()
    {
        Score += AddScoreForLet;
        EventScoreUp?.Invoke(Score);
    }
}
