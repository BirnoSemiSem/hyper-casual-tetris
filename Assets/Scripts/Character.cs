using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    [SerializeField] private Rigidbody _rb;

    [SerializeField] private GameObject Player;

    public Transform CameraPosition;
    public Transform CameraLookAt;

    [SerializeField] private float _speed;

    [SerializeField] private float _speedNormal;
    [SerializeField] private float _speedBoostMax;

    [SerializeField] private float _powerKnockback;

    public bool IsCheckMouseX = false;
    public bool IsBoost = false;

    private float LastClickTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        if (_rb == null)
            _rb = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        GameController.EventFinishGame += FinishGame;
    }

    private void OnDisable()
    {
        GameController.EventFinishGame -= FinishGame;
    }

    void FixedUpdate()
    {
        if (!GameController.IsGame)
            return;

        CheckMouse();

        Move();

        PlayerRotation();

        transform.position = new Vector3(transform.position.x, 0f, transform.position.z);
    }

    void CheckMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            IsCheckMouseX = true;

            float timeFromLastClick = Time.time - LastClickTime;
            LastClickTime = Time.time;

            if (timeFromLastClick < 0.5)
                IsBoost = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            IsBoost = false;
            IsCheckMouseX = false;
        }
    }

    void Move()
    {
        if(IsBoost)
            _rb.velocity = _rb.velocity.z < _speedBoostMax ? _rb.velocity + new Vector3(0, 0, _speed * Time.fixedDeltaTime) : new Vector3(0, 0, _speedBoostMax);
        else
            _rb.velocity = _rb.velocity.z < _speedNormal ? _rb.velocity + new Vector3(0, 0, _speed * Time.fixedDeltaTime) : new Vector3(0, 0, _speedNormal);
    }

    void PlayerRotation()
    {
        if (IsCheckMouseX)
        {
            Quaternion quaternion = Player.transform.rotation;

            quaternion.y += Input.GetAxis("Mouse X") * Time.deltaTime;

            Player.transform.rotation = quaternion;
        }
    }

    void FinishGame()
    {
        _rb.velocity = Vector3.zero;
    }

    public void PlayerKnockback()
    {
        _rb.AddForce(Vector3.back * _powerKnockback, ForceMode.Impulse);
        _rb.velocity = Vector3.zero;
    }
}
